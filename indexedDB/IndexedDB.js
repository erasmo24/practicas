// instancia de la base de datos
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var dataBase = null;

// metodo que abre la base de datos 
function starDB () {
	dataBase = indexedDB.open("prueba1", 1);

	dataBase.onupgradeneeded = function (e) {
		active = dataBase.result;

		// createObjectStore
 		// este metodo crea un store que es una especie de tabla 
 		tablaPeople = active.createObjectStore("people", {keyPath: 'id', autoIncrement : true});

 		// esto metodo crea un indice que es una especi de columna en sql
 		tablaPeople.createIndex('by_name', 'name', {unique:false});
 		tablaPeople.createIndex('by_apellido', 'lastname', {unique:false});
 		tablaPeople.createIndex('by_dni', 'dni', {unique:true});
	};

	// este metodo verifica la base de datos si todo esta bien muestra el codigo dentro del
	dataBase.onsuccess = function(e){
		alert('La base de datos esta ON');
	}

	// este metodo se dispara si hubo un error al intanciar la base de datos
	dataBase.onerror = function (e) {
		alert('Hubo un erro al encender la DB');
	}

}

// esta funcion agrega un registro a la tabla
function agregar () {
	var active = dataBase.result;

	// todo debe hacerce bajo una transacion, por eso el metodo transaction
	var data = active.transaction(["people"], "readwrite");

	// este metodo especifica el almacen o store(tabla) que se le va a insertar el registro
	var object = data.ObjectStore("people");

	var datos = {
		dni : document.querySelector("#dni").value,
		name : document.querySelector("#nombre").value,
		lastname : document.querySelector("#apellido").value

	};

	var insertar = object.add(datos);

	if (insertar.onerror()) {
		alert(insertar.error.name + '\n\n' + insertar.error.mensage);
	};
}

